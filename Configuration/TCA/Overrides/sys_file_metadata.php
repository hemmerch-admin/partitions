<?php
if (!defined('TYPO3_MODE')) die ('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file_metadata', 'voice', '', 'before:description');

$tca = array(
	'columns' => array(
		'voice' => array(
			'label' => 'Voix',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', ''),
					array('Alto', '1'),
					array('Soprano', '2'),
					array('Ténor', '3'),
					array('Basse', '4'),
					array('Toutes', '5'),
				),
				'maxitems' => 1,
				'minitems' => 0,
			),
		),
	),
);
\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TCA']['sys_file_metadata'], $tca);