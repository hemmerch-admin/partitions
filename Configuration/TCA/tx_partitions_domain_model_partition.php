<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

return array(
	'ctrl' => array(
		'title' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:partition',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,composer',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('partitions') . 'Resources/Public/Icons/tx_partitions_domain_model_partition.gif'
	),
	'types' => array(
		#sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource
		'1' => array(
			'showitem' => '--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.partition;partition,
				--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.people;people,
				--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.general_informations;general_informations,
				--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.numbers;numbers,
				--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.liturgy;liturgy,
				--palette--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:palettes.appreciation;appreciation,
				--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.audio, audio_files,
				--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.partitions, partition_files,
				--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.contributors, contributors,
				--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.relations, related_partitions,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime'
		),
	),
	'palettes' => array(
		'partition' => array(
			'showitem' => 'title, --linebreak--, internal_identifier',
			'canNotCollapse' => TRUE
		),
		'people' => array(
			'showitem' => 'composer, --linebreak--, composer_origin, --linebreak--, author, --linebreak--, arranger, --linebreak--, editor, --linebreak--, reference',
			'canNotCollapse' => TRUE
		),
		'general_informations' => array(
			'showitem' => 'formation, --linebreak--, voice, --linebreak--, theme, --linebreak--, accompaniment, --linebreak--, video_type, video, --linebreak--, remark',
			'canNotCollapse' => TRUE
		),
		'numbers' => array(
			'showitem' => 'year_of_composition, --linebreak--, publication_year, --linebreak--, duration',
			'canNotCollapse' => TRUE
		),
		'liturgy' => array(
			'showitem' => 'liturgy_function, --linebreak--, liturgical_season',
			'canNotCollapse' => TRUE
		),
		'appreciation' => array(
			'showitem' => 'difficulty, --linebreak--, comment_difficulty, --linebreak--, writing_quality, --linebreak--, comment_writing_quality',
			'canNotCollapse' => TRUE
		),
	),

	'columns' => array(
		'sys_language_uid' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_partitions_domain_model_partition',
				'foreign_table_where' => 'AND tx_partitions_domain_model_partition.pid=###CURRENT_PID### AND tx_partitions_domain_model_partition.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'internal_identifier' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:internal_identifier',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),

		'composer' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'composer_origin' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer_origin',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'author' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:author',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'arranger' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:arranger',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'editor' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:editor',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'reference' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:reference',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'formation' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:formation',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'voice' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:voice',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'theme' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:theme',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'accompaniment' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:accompaniment',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'video' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:video',
			'config' => array(
				'type' => 'input',
				'size' => '30',
				'eval' => 'trim',
				'wizards' => array(
					'_PADDING' => 2,
					'link' => array(
						'type' => 'popup',
						'title' => 'LLL:EXT:cms/locallang_ttc.xml:header_link_formlabel',
						'icon' => 'link_popup.gif',
						'script' => 'browse_links.php?mode=wizard',
						'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1',
						'params' => array (
							'blindLinkOptions' => 'folder,mail,page,spec',
							'allowedExtensions' => 'mp4,ogg,webm',
						),
					),
				),
				'softref' => 'typolink',
			),
		),
		'remark' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:remark',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'publication_year' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:publication_year',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'trim'
			),
		),
		'year_of_composition' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:year_of_composition',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		/*'modification_date' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:modification_date',
			'config' => array(
				'dbType' => 'datetime',
				'type' => 'input',
				'size' => 4,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => '0000-00-00 00:00:00'
			),
		),*/
		'duration' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:duration',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'trim'
			),
		),

		'liturgy_function' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:liturgy_function',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'liturgical_season' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:liturgical_season',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),

		'difficulty' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:difficulty',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', ''),
					array('* * * * *', 5),
					array('* * * *', 4),
					array('* * *', 3),
					array('* *', 2),
					array('*', 1),
				),
				'maxitems' => 1,
				'minitems' => 0,
			),
		),
		'comment_difficulty' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:comment_difficulty',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 3,
				'eval' => 'trim'
			),
		),
		'writing_quality' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:writing_quality',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', ''),
					array('* * * * *', 5),
					array('* * * *', 4),
					array('* * *', 3),
					array('* *', 2),
					array('*', 1),
				),
				'maxitems' => 1,
				'minitems' => 0,
			)
		),
		'comment_writing_quality' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:comment_writing_quality',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 3,
				'eval' => 'trim'
			),
		),

		'related_partitions' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:related_partitions',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_partitions_domain_model_partition',
				'MM' => 'tx_partitions_partition_partition_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_partitions_domain_model_partition',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),

		'partition_files' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:partition_files',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'partition_files',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:addPartitionFileReference'
					),
					'minitems' => 0,
					'maxitems' => 99,
				),
				'pdf'
			),
		),
		'audio_files' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:audio_files',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'audio_files',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:addAudioFileReference'
					),
					'minitems' => 0,
					'maxitems' => 99,
				),
				'mp3,mid'
			),
		),
		'contributors' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:contributors',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_partitions_domain_model_contributor',
				#'foreign_field' => 'partitions',
				'MM' => 'tx_partitions_partition_contributor_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
			),
		),
	),
	'grid' => array(
		'facets' => array(
			'uid',
			'title',
			'composer',
			'arranger',
			'author',
			'voice',
			'accompaniment',
			new \TYPO3\CMS\Vidi\Facet\StandardFacet(
				'composer_origin',
				'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer_origin_fe'
			),
			new \TYPO3\CMS\Vidi\Facet\StandardFacet(
				'difficulty',
				'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:difficulty',
				array(
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
				)
			),
			new \TYPO3\CMS\Vidi\Facet\StandardFacet(
				'writing_quality',
				'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:writing_quality',
				array(
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
				)
			),
			'liturgy_function',
			'liturgical_season',
			new \TYPO3\CMS\Vidi\Facet\StandardFacet(
				'reference',
				'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:reference_fe'
			),
		),
		'columns' => array(
			'__checkbox' => array(
				'renderer' => new \TYPO3\CMS\Vidi\Grid\CheckBoxComponent(),
			),
			'uid' => array(
				'visible' => FALSE,
				'label' => 'Id',
				'width' => '5px',
			),
			'title' => array(
				'editable' => TRUE,
			),
//			'contributors' => array(
//				'visible' => TRUE,
//				'renderers' => array(
//					'TYPO3\CMS\Vidi\Grid\RelationEditRenderer',
//					'TYPO3\CMS\Vidi\Grid\RelationRenderer',
//				),
//				'editable' => TRUE,
//				'sortable' => FALSE,
//			),
			'__buttons' => array(
				'renderer' => new \TYPO3\CMS\Vidi\Grid\ButtonGroupComponent(),
			),
		)
	),

	'grid_frontend' => array(
		'columns' => array(

			# The field "title" of your table.
			'title' => array(),
			'voice' => array(
				'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:voice_fe'
			),
			'author' => array(
				'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:author_fe'
			),
			'composer' => array(
				'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer_fe'
			),
			'arranger' => array(
				'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:arranger_fe'
			),
			'partition_files' => array(
				'renderer' => new \Hemmer\Partitions\Grid\HasValueGridComponent(),
				'sortable' => FALSE,
				'label' => 'Partition'
			),
			'audio_files' => array(
				'renderer' => new \Hemmer\Partitions\Grid\HasValueGridComponent(),
				'sortable' => FALSE,
				'label' => 'Enreg.'
			),
			'contributors' => array(
				'renderer' => new \Hemmer\Partitions\Grid\HasValueGridComponent(),
				'sortable' => FALSE,
				'label' => 'Travail'
			),
			'video' => array(
				'renderer' => new \Hemmer\Partitions\Grid\VideoGridComponent(),
				'sortable' => FALSE,
				'label' => 'Travail'
			),
			'writing_quality' => array(
				'renderer' => new \Hemmer\Partitions\Grid\RatingGridComponent(),
			),
			'crdate' => array(
				'format' => 'TYPO3\CMS\Vidi\Formatter\Date',
				'label' => 'Publié',
			),

			# System column where to contain some
			'__buttons' => array(
				'renderer' => 'Fab\VidiFrontend\Grid\ShowButtonRenderer',
				'sortable' => FALSE
			),
		),
	),

);

