
plugin.tx_partitions {
	view {
		# cat=plugin.tx_partitions/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:partitions/Resources/Private/Templates/
		# cat=plugin.tx_partitions/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:partitions/Resources/Private/Partials/
		# cat=plugin.tx_partitions/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:partitions/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_partitions//a; type=string; label=Default storage PID
		storagePid =
	}
}
