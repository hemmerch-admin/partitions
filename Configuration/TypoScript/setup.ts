
plugin.tx_partitions {
	view {
		templateRootPath = {$plugin.tx_partitions.view.templateRootPath}
		partialRootPath = {$plugin.tx_partitions.view.partialRootPath}
		layoutRootPath = {$plugin.tx_partitions.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_partitions.persistence.storagePid}
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
}

plugin.tx_vidifrontend {
	settings {
		templates {
			# Use key "10", "11" and following for your own templates
			10 {
				title = Partition detail view
				path = EXT:partitions/Resources/Private/Templates/VidiFrontend/ShowPartition.html
			}
		}

		# Fluid variable mappings to be used in the detail view of your Fluid template.
		fluidVariables {
			tx_partitions_domain_model_partition = partition
		}
	}

	view {
		partialRootPath = EXT:partitions/Resources/Private/Partials/
	}
}

