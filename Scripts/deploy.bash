#!/bin/bash
#
# This script is used to deploy on the remote since there is no git available.
#

# Adjust me!
extensions=( "partitions" "vidi_frontend" "vidi" )
#extensions=( "partitions" )

# Initialize empty variable
deploy=""
copy=""

# Move the cursor to the proper place
cd ~/Sites/Hemmer/chant.ch/htdocs/typo3conf/ext/

# Loop around extensions and prepare tarballs
for (( i = 0 ; i < ${#extensions[@]} ; i++ )) do
	extension=${extensions[$i]}
	if [ $extension ]; then
		echo "Exporting $extension..."
		tar -czf $extension.tar.gz --exclude='.*' --exclude='node_modules' --exclude='Resources/Public/WebComponents' $extension
		copy="$copy $extension.tar.gz"
		deploy="$deploy tar -xzf $extension.tar.gz; rm -rf /httpdocs/typo3conf/ext/$extension; mv $extension /httpdocs/typo3conf/ext/; rm $extension.tar.gz; "
    fi
done

# Work...
echo "Deploy on the remote..."

# Real work (comment me out!)
#scp $copy chant:/tmp
#rm $copy
#ssh chant "cd /tmp; $deploy"

# Debug
echo "scp $copy chant:/tmp"
echo "rm $copy"
echo "cd /tmp; $deploy"

## rm -rf /httpdocs/typo3temp/Cache/
