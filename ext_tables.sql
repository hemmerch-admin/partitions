#
# Table structure for table 'tx_partitions_domain_model_partition'
#
CREATE TABLE tx_partitions_domain_model_partition (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	composer varchar(255) DEFAULT '' NOT NULL,
	composer_origin varchar(255) DEFAULT '' NOT NULL,
	year_of_composition int(11) DEFAULT '0' NOT NULL,
	formation varchar(255) DEFAULT '' NOT NULL,
	theme varchar(255) DEFAULT '' NOT NULL,
	modification_date datetime DEFAULT '0000-00-00 00:00:00',
	author varchar(255) DEFAULT '' NOT NULL,
	arranger varchar(255) DEFAULT '' NOT NULL,
	publication_year varchar(255) DEFAULT '' NOT NULL,
	editor varchar(255) DEFAULT '' NOT NULL,
	reference varchar(255) DEFAULT '' NOT NULL,
	duration varchar(255) DEFAULT '' NOT NULL,
	remark varchar(255) DEFAULT '' NOT NULL,
	liturgy_function varchar(255) DEFAULT '' NOT NULL,
	comment_writing_quality varchar(255) DEFAULT '' NOT NULL,
	comment_difficulty varchar(255) DEFAULT '' NOT NULL,
	internal_identifier int(11) DEFAULT '0' NOT NULL,
	partition_files int(11) unsigned NOT NULL default '0',
	audio_files int(11) unsigned NOT NULL default '0',
	difficulty int(11) DEFAULT '0' NOT NULL,
	writing_quality int(11) DEFAULT '0' NOT NULL,
	voice varchar(255) DEFAULT '' NOT NULL,
	liturgical_season varchar(255) DEFAULT '' NOT NULL,
	accompaniment varchar(255) DEFAULT '' NOT NULL,
	video varchar(255) DEFAULT '' NOT NULL,
	contributors int(11) unsigned DEFAULT '0' NOT NULL,
	related_partitions int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_partitions_domain_model_contributor'
#
CREATE TABLE tx_partitions_domain_model_contributor (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	audio_files int(11) unsigned DEFAULT '0' NOT NULL,
	partitions int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_partitions_partition_contributor_mm'
#
CREATE TABLE tx_partitions_partition_contributor_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_partitions_partition_partition_mm'
#
CREATE TABLE tx_partitions_partition_partition_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'sys_file_metadata'
#
CREATE TABLE sys_file_metadata (
	voice varchar(255) DEFAULT '' NOT NULL,
);