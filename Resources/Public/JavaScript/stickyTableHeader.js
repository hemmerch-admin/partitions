jQuery(function($) {
	$(document).ready(function() {
		var mainTop = $('#main').position().top + 56,
			dataTablesWrapper = '.dataTables_wrapper',
			stickyClass = 'sticky';

		/* Resizes table cells */
		var resizeTableCells = function() {
			$(dataTablesWrapper + ' tbody tr').each(function() {
				$(this).find('td').each(function(i) {
					var count = i + 1;

					$(this).width($(this).parents('table').find('th:nth-of-type(' + count + ')').width());
				});
			});
		};

		/* Sticks table header on top */
		var stickTableHeader = function() {
			if ($(dataTablesWrapper).length) {
				var dataTablesWrapperTop = $(dataTablesWrapper).position().top - $(window).scrollTop();

				if (dataTablesWrapperTop <= mainTop) {
					if (!$(dataTablesWrapper).hasClass(stickyClass)) {
						resizeTableCells();
						$(dataTablesWrapper).addClass(stickyClass);
					}
				} else {
					if ($(dataTablesWrapper).hasClass(stickyClass)) {
						$(dataTablesWrapper).removeClass(stickyClass);
					}
				}
			}
		};

		/* Checks if data is no more processing */
		var checkDataProcessing = function() {
			if (!$(dataTablesWrapper + ' .dataTables_processing').is(':visible')) {
				resizeTableCells();
			} else {
				setTimeout(checkDataProcessing, 200);
			}
		}

		stickTableHeader();
		resizeTableCells();

		$(window).scroll(function() {
			stickTableHeader();
		});

		$(dataTablesWrapper + ' th').on('click', function() {
			checkDataProcessing();
		})
	});
});