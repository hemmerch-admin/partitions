<?php
namespace Hemmer\Partitions\ViewHelpers\File;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * View helper which returns the video to render
 */
class VideoViewHelper extends AbstractViewHelper {
	/**
	 * Initializes the arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument('video', 'string', 'The given video');
	}

	/**
	 * Return the rendered video.
	 *
	 * @return string $returnedPath
	 */
	public function render() {
		$video = $this->arguments['video'];
		$fileCheck = 'file:';
		$youtubeCheck = 'youtube.com';
		$renderedVideo = '';

		/* Link of type FAL */
		if ($fileCheck === substr($video, 0, strlen($fileCheck))) {
			$videoLink = $GLOBALS['TSFE']->cObj->typoLink(
				'',
				array(
					'parameter' => $video,
					'returnLast' => 'url'
				)
			);

			$renderedVideo = '
				 <video width="100%" height="auto" preload="metadata" controls>
					<source src="' . $videoLink . '" type="video/mp4">
					Balise vidéo pas supportée.
				</video>
			';

		/* Youtube link */
		} elseif (strpos($video, $youtubeCheck) !== FALSE) {
			parse_str(parse_url($video, PHP_URL_QUERY), $videoParameters);
			$videoLink = 'https://www.youtube.com/embed/' . trim(explode('-', $videoParameters['v'], 2)[0]) . '?rel=0';

			$renderedVideo = '
				<iframe width="100%" height="auto"
					src="' . $videoLink . '"
					frameborder="0" allowfullscreen />
			';
		}

		return $renderedVideo;
	}

}
