<?php
namespace Hemmer\Partitions\Grid;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Vidi\Grid\GridRendererAbstract;

/**
 * Class for rendering a Video in the Grid.
 */
class RatingRenderer extends GridRendererAbstract {

	/**
	 * Renders a Video in the Grid.
	 *
	 * @return string
	 */
	public function render() {

		$output = '';
		if ($this->object[$this->fieldName]) {
			$stars = GeneralUtility::trimExplode(' ', $this->object[$this->fieldName], TRUE);
			foreach ($stars as $star) {
				$output .= '<i class="fa fa-sm fa-star"></i> ';
			}
		}

		return $output;
	}
}
