<?php
namespace Hemmer\Partitions\Grid;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Vidi\Grid\GridRendererAbstract;

/**
 * Class for telling whether the given field name has associated values.
 */
class HasValueRenderer extends GridRendererAbstract {

	/**
	 * Tells whether the given field name has associated values.
	 *
	 * @return string
	 */
	public function render() {

		$output = sprintf(
			'<span class="label label-%s">&nbsp;&nbsp;&nbsp;</span>',
			empty($this->object[$this->fieldName]) ? 'danger' : 'success'

		);
		return $output;
	}
}
