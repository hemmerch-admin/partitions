<?php
namespace Hemmer\Partitions\Grid;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Vidi\Grid\GridRendererAbstract;

/**
 * Class for rendering a Video in the Grid.
 */
class VideoRenderer extends GridRendererAbstract {

	/**
	 * Renders a Video in the Grid.
	 *
	 * @return string
	 */
	public function render() {

		$output = '';
		if ($this->object[$this->fieldName]) {
			$output = '<span class="label label-video">&nbsp;&nbsp;&nbsp;</span>';
		}

		return $output;
	}
}
