<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_partitions_domain_model_partition');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_partitions_domain_model_contributor');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable('partitions', 'tx_partitions_domain_model_partition');


// Register some Vidi modules.

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

/** @var \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility $configurationUtility */
$configurationUtility = $objectManager->get('TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility');
$configuration = $configurationUtility->getCurrentConfiguration('vidi');

$dataTypes = array('tx_partitions_domain_model_partition');

// Loop around the data types and register them to be displayed within a BE module.
foreach ($dataTypes as $dataType) {

	// Compute file path
	$iconFile = sprintf('EXT:partitions/Resources/Public/Images/%s.png', $dataType);
	$languageFile = sprintf('LLL:EXT:partitions/Resources/Private/Language/%s.xlf', $dataType);

	/** @var \TYPO3\CMS\Vidi\ModuleLoader $moduleLoader */
	$moduleLoader = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Vidi\ModuleLoader', $dataType);
	$moduleLoader->setIcon($iconFile)
		->setModuleLanguageFile($languageFile)
		->setDefaultPid($configuration['default_pid']['value'])
		->register();
}