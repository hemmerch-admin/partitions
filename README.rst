Musical Partition Management
============================

This is a extension for TYPO3 CMS 6.2 and above which provides a bunch of Domain Model for handling a set of Partitions.

Provides a bunch of Domain Model for handling a set of musical Partitions.

Installing the extension
------------------------

Install in the Extension Manager as normal. That's it!


Load assets files (JS / CSS)
----------------------------

In order the plugin to work, it is required to load some CSS and JavaScript::

	# CSS
	EXT:partitions/Resources/Public/StyleSheets/partitions.css

	# JavaScript
	EXT:partitions/Resources/Public/JavaScript/partitions.js

Scripts
-------

There is a script to deploy on the server since there is not Git available on the production.

::

	cd Scripts
	./deploy.bash


TODO
====

* Fix list view
* Fix detail view (arrow, relatedPartitions, ...)
* Configure BE module the same way as on the Frontend
* Check whether to use HTML5 audio.
